'use strict'

// ES5 to ES6 migration

/* Utils function @TODO export that from a separate utils.js file and import it here */
var utils = {
/**
 * Truncates the decimal part of a number to a given number of decimal places.
 * @param {number} num - Input number
 * @param {number} dec - Number of decimals to keep
 * @returns {string} A string representation of the truncated number with the specified decimal places.
 */
  truncDecimals: function (num, dec) {
    return Intl.NumberFormat('en', {
      maximumFractionDigits: dec,
      roundingMode: 'trunc'
    }).format(num)
  },
/**
 * Sums the elements in an array.
 * @param {Array<number>} arr - The array of numbers to sum.
 * @returns {number} The sum of the elements in the array.
 */
  arraySum: arr => arr.reduce((total, value) => total + value)
}

var logger = {
  onOperationSuccess: function(title) {
    console.log(`Internal: successful operation on book: ${title}`)
  },
  onOperationFailed: function(title) {
    console.log(`Internal: operation failed on book: ${title}`)
  },
  userHasNoSubscription: function() {
    console.log('the user has no subscription')
  },
  bookIsBorrowed: function(title) {
    console.log(`${title} is already borrowed`)
  },
  bookDoesNotExist: function(name) {
    console.log(`${name} does not exist`)
  },
  giveBooksToTheReader: function(num) {
    console.log(`Thank you. Here are your ${num} books`)
  },
  separator: function() {
    console.log('//////////////////////////////////////////////')
  }
}

/**
 * A book in the library.
 *
 * @class
 * @constructor
 * @param {number} id - The id of the book.
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 * @param {string} name - The instance name of the book.
 */
class Book {
  constructor(id, title, author, name) {
    this.id = id
    this.title = title
    this.name = name
    this.author = author
    this.comments = []
    this.revision = 0
    this.evaluations = []
    this.borrowed = false

    library.addBook(this)
  }

  getId() {
    return this.id
  }
  getTitle() {
    return this.title
  }
  getAuthor() {
    return this.author
  }
  getName() {
    return this.name
  }
  setComment(str) {
    return this.comments.push(str)
  }
  getComments() {
    return this.comments
  }
  setBorrowed(bool) {
    return this.borrowed = bool
  }
  getBorrowed() {
    return this.borrowed
  }
  setEvaluation(num) {
    return this.evaluations.push(num)
  }
  getGlobalEvaluation() {
    return utils.truncDecimals(utils.arraySum(this.evaluations) / this.evaluations.length, 3) // round with 3 decimals
  }
}

/**
 * Creates a new Reader.
 * @class
 * @constructor
 * @param {boolean} subscribed - Indicates if the reader is subscribed.
 * @returns {object} An object with methods to interact with the reader.
 */
class Reader {
  constructor(subscribed) {
    this.isSubscribed = subscribed
    this.borrowedBooks = []
  }

  hasValidSubscription() {
    return this.isSubscribed
  }

  getBorrowedBooks() {
    return this.borrowedBooks
  }

  addBooks(books) {
    this.borrowedBooks.push(books)
  }
}

/**
 * Represents a library object that manages a collection of books and allows readers to borrow them.
 * @class
 * @constructor
 * @param {Array<Book>} books - An array of book objects to initialize the library with.
*/
class Library {
  constructor(books) {
    this.books = books
  }

  addBook = function(book) {
    this.books.push(book)
  }

/**
 * Removes a book with the specified id from the library's collection.
 * If the book is currently borrowed by a reader, it cannot be removed.
 * using [0] after splice get the book object itself rather than an array containing the book object
 * @param {number} id - The id of the book to be removed.
 * @returns {Book|undefined} - The removed book object or undefined if the book was not found or is currently borrowed.
 */
  removeBook = function(id) {
    const index = this.books.findIndex(book => book.getId() === id && book.getBorrowed() === false)
    if (index !== -1) {
      return this.books.splice(index, 1)[0]
    }
  }

  /**
   * Search for books based on their titles.
  /*
  In this code, Array.filter() is used to filter out the books
  that don't match the search criteria,
  and Array.map() is used to extract the names of the matched books.
  The Array.from() method is used to convert the arguments object to an array
  so that it can be used with the Array.every() method,
  which returns true if every element in the array passes a certain test.
  This code will be more efficient because it avoids unnecessary iterations
  and reduces the number of conditional statements.
 * @param {...string} titles - The titles of the books to search for.
 * @returns {string[]} An array of book names that match all the given titles.
 */
  searchBooks = function() {
    var args = arguments
    var books = this.books.filter(function(book) {
      return Array.from(args).every(function(arg) {
        return book.getTitle().includes(arg)
      })
    }).map(function(book) {
      return book.getName()
    })

    return books
  }

/**
 * Finds the index of a book in the library by name.
 *
 * @param {string} name - The name of the book to find.
 * @returns {number} The index of the book in the library, or -1 if not found.
 */
  findBookIndexByName = function(name) {
    return this.books.findIndex(book => book.getName() === name)
  }

  giveBarrowedBooksToTheReader = function(user, books) {
    user.addBooks(books)
    logger.giveBooksToTheReader(books.length)
    console.log(user.getBorrowedBooks())
  }

  barrowBook = async function(user, books) { // livre emprunté
    var booksLength = books.length
    var borrowedBooks = [] // Array of borrowed books to pass to reader object

    async function checkResult(book, success) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (success) { // user.hasValidSubscription()
            var name = book.getName()
            var bookIndex = this.findBookIndexByName(name)
            var title = book.getTitle()
            var bookIsBorrowed = book.getBorrowed()

            if (bookIndex === -1) { // book is not in library.books
              logger.bookDoesNotExist(name)
            } else if (bookIsBorrowed) { // book is already borrowed
              logger.bookIsBorrowed(title)
            } else { // OK borrow
              book.setBorrowed(true)
              borrowedBooks.push(book)
              logger.onOperationSuccess(title)
            }
          } else { // user has no subscription
            logger.userHasNoSubscription()
          }
          logger.separator()
          resolve();
        }, 2000);
      });
    }

    for (var i = 0; i < booksLength; i++) {
      var book = books[i]
      await checkResult.bind(this)(book, user.hasValidSubscription())
    }

    logger.separator()
    this.giveBarrowedBooksToTheReader(user, borrowedBooks)
  }

}

// ---------------------------------------------

var library = new Library([])

var bookA = new Book(101, 'La Barbe Bleue', 'Charles Perrault', 'bookA')
var bookB = new Book(32, 'La Petite Poucette', 'Hans Chistian Andersen', 'bookB')
var bookC = new Book(35, 'La Petite Sirene', 'Hans Chistian Andersen', 'bookC')
var bookD = new Book(67, 'Hansel et Gretel', 'Jacob et Wilhelm Grimm', 'bookD')
var bookE = new Book(75, 'La Petite gardeuse d\'oies', 'Jacob et Wilhelm Grimm', 'bookE')
var bookF = new Book(3, 'Baba Yaga', 'Alexandre Afanassiev', 'bookF')

var reader1 = new Reader(true)

bookC.setEvaluation(5)
bookC.setEvaluation(4)
bookC.setEvaluation(5)
console.log(bookC.getGlobalEvaluation()) // output: 4.666
library.removeBook(101) // bookA is removed
console.log(library.searchBooks('Petite')) // output: [bookB, bookC, bookE]
console.log(library.searchBooks('Petite', 'Sirene')) // output: [bookC]

library.barrowBook(reader1, [bookF, bookD, bookA, bookD])
/* output:
Internal: successful operation on book: Baba Yaga
Internal: successful operation on book: Hansel et Gretel
Thank you. Here are your 2 books */
