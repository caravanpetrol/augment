'use strict'

// Books management in a library with ES5 corrections, modifications and optimizations
// A Book is defined by an id (integer), a title (string) and a author (string).
// It can be commented (string) and get evaluations (integer between 0 and 5). Internal revision is used for future purpose.

/* Utils function @TODO export that from a separate utils.js file and import it here */
var utils = {
/**
 * Format number whith specific num of decimals
 * @param {Number} num Input number
 * @param {Number} dec Number of decimals to output
 * @returns {String}
 */
  truncDecimals: function (num, dec) {
    return Intl.NumberFormat('en', {
      maximumFractionDigits: dec,
      roundingMode: 'trunc'
    }).format(num)
  },
/**
 * Addition array of numbers
 * @param {Array} arr
 * @returns {Number}
*/
  arraySum: function(arr) {
    function fn(total, value) {
      return total + value;
    }
    return arr.reduce(fn)
  }
}

// var logger = createLogger()


/**
 * Book constructor - Create with the 'new' operator
 * The value of 'this' will be the new object created when the function is invoked
 * If a create methods here, all of them will be duplicated in every new created instances, which is not memory efficient
 * @param {Number} id
 * @param {String} title
 * @param {String} author
 * @param {String} name
 */
function Book(id, title, author, name) {
  // Private members
  this._id = id
  this._title = title
  this._author = author
  this._name = name
  this._comments = []
  this._revision = 0
  this._evaluations = []
  this._borrowed = false

  library.addBook(this);
}

/* Adding functions to the constructor's prototype will be shared by all new instances
   New books don't have a method of their own, JavaScript follows the prototype linkage and finds them on the Book.prototype object
 */
Book.prototype.getId = function () {
  return this._id
}
Book.prototype.getTitle = function () {
  return this._title
}
Book.prototype.getAuthor = function () {
  return this._author
}
Book.prototype.getName = function () {
  return this._name
}
Book.prototype.setComment = function (str) {
  return this._comments.push(str)
}
Book.prototype.getComments = function () {
  return this._comments
}
Book.prototype.setBorrowed = function (bool) {
  return this._borrowed = bool
}
Book.prototype.getBorrowed = function () {
  return this._borrowed
}
Book.prototype.setEvaluation = function (num) {
  return this._evaluations.push(num)
}
Book.prototype.getGlobalEvaluation = function () {
  return utils.truncDecimals(utils.arraySum(this._evaluations) / this._evaluations.length, 3) // round with 3 decimals
}

/**
 * Library constructor
 * @returns {Object}
 */
function Library() {
  this._books = [];

  this.addBook = function(book) {
    // @TODO check if book already exist, if not :
    this._books.push(book)
  }

  this.removeBook = function(id) {
    for (var i = 0; i < this._books.length; ++i) {
      if (this._books[i].getId() === id) {
        if(this._books[i].getBorrowed() === false) { // check if book is not borrowed
          return this._books.splice(i, 1);
        }
      }
    }
  }

  this.searchBooks = function() {
    var args = arguments;
    var books = [];

    for (var i = 0; i < this._books.length; ++i) {
      var title = this._books[i].getTitle();
      var matches = 0;
      for (var k = 0; k < args.length; ++k) {
        if (title.indexOf(args[k]) != -1) {
          matches += 1;
        }
      }

      if (matches == args.length) {
        books.push(this._books[i].getName());
      }
    }

    return books;
  }

  this.findBookIndexByName = function(name) {
    for(var i = 0; i < this._books.length; ++i) {
      if (this._books[i].getName() === name) {
        return i
      }
    }
    return -1
  }

  this.giveBarrowedBooksToTheReader = function(user, books) {
    user.addBooks(books)
    logger.separator()
    logger.giveBooksToTheReader(books.length)
    logger.m(reader1.borrowedBooks())
  }

  this.barrowBook = function(user, books) { // livre emprunté
    var booksLength = books.length
    var processedBooks = 0; // keep track of the number of processed books
    var borrowedBooks = []; // Array of borrowed books to pass to reader object
    function checkResult(book, success) {
      processedBooks++; // increment the processed books counter
      if (success) { // user.hasValidSubscription()
        var name = book.getName()
        var bookIndex = this.findBookIndexByName(name)
        var title = book.getTitle()
        var bookIsBorrowed = book.getBorrowed()

        if (bookIndex === -1) { // book is not in library._books
          logger.separator()
          logger.bookDoesNotExist(name)
        } else if (bookIsBorrowed) { // book is already borrowed
          logger.separator()
          logger.bookIsBorrowed(title)
        } else { // OK borrow
          logger.separator()
          book.setBorrowed(true)
          borrowedBooks.push(book);
          logger.onOperationSuccess(title);
        }
      } else { // user has no subscription
        logger.separator()
        logger.userHasNoSubscription()
      }
      if (processedBooks === booksLength) { // check if all books have been processed
        this.giveBarrowedBooksToTheReader(user, borrowedBooks);
      }
    }

    for (var i = 0; i < booksLength; i++) {
      var book = books[i];
      setTimeout(checkResult.bind(this), 3000*i, book, user.hasValidSubscription());
    }
  }
}

var logger = {
  m: function(message) {
    console.log(message)
  },
  separator: function() {
    console.log('//////////////////////////////////////////////')
  },
  onOperationSuccess: function(title) {
    console.log('Internal: successful operation on book: ' + title)
  },
  onOperationFailed: function(title) {
    console.log('Internal: operation failed on book: ' + title)
  },
  userHasNoSubscription: function() {
    console.log('the user has no subscription')
  },
  bookIsBorrowed: function(title) {
    console.log(title + ' is already borrowed')
  },
  bookDoesNotExist: function(name) {
    console.log(name + ' does not exist')
  },
  giveBooksToTheReader: function(num) {
    console.log('Thank you. Here are your ' + num + ' books')
  }
}


/**
 * Reader Constructor - instanciate with a valid suscription (or not)
 * @param {Boolean} subscribed
 * @returns {Object}
 */
function Reader(subscribed) {
  var isSubscribed = subscribed
  var borrowedBooks = []

  return {
    hasValidSubscription: function() {
      return isSubscribed
    },
    borrowedBooks: function() {
      return borrowedBooks
    },
    addBooks: function(books) {
      borrowedBooks.push(books)
    }
  }
}
// ---------------------------------------------

var library = new Library();

var bookA = new Book(101, 'La Barbe Bleue', 'Charles Perrault', 'bookA');
var bookB = new Book(32, 'La Petite Poucette', 'Hans Chistian Andersen', 'bookB');
var bookC = new Book(35, 'La Petite Sirene', 'Hans Chistian Andersen', 'bookC');
var bookD = new Book(67, 'Hansel et Gretel', 'Jacob et Wilhelm Grimm', 'bookD');
var bookE = new Book(75, 'La Petite gardeuse d\'oies', 'Jacob et Wilhelm Grimm', 'bookE');
var bookF = new Book(3, 'Baba Yaga', 'Alexandre Afanassiev', 'bookF');

var reader1 = new Reader(true)
var reader2 = new Reader(false)

bookC.setEvaluation(5);
bookC.setEvaluation(4);
bookC.setEvaluation(5);
console.log(bookC.getGlobalEvaluation()); // output: 4.666
library.removeBook(101); // bookA is removed
console.log(library.searchBooks('Petite')); // output: [bookB, bookC, bookE]
console.log(library.searchBooks('Petite', 'Sirene')); // output: [bookC]

library.barrowBook(reader1, [bookF, bookD, bookA])
/* output:
Internal: successful operation on book: Baba Yaga
Internal: successful operation on book: Hansel et Gretel
Thank you. Here are your 2 books
library.barrowBook(reader2, [bookB, bookE]) */

logger.separator()
logger.m(library)




