/* ****************************************************
/                      INSTRUCTIONS
/ Here is the code corresponding a simplified management of books in a library.
/ Elements are declared first then used in a short example.
/
/ Please, read and complete the following tasks:
/ 1/ Review and prepare comments on the following code. Note all your remarks. They will be debriefed during an interview.
/ 2/ Write a new script named es5.js written in ES5 with corrections, modifications and optimizations that you find pertinent.
/ 3/ Write a new script named es6.js by migrating your modified code (es5.js) to ES6
/
**************************************************** */


/* ********************** DECLARATIONS ****************************** */

// A Book is defined by an id (integer), a title (string) and a author (string).
// It can be commented (string) and get evaluations (integer between 0 and 5). Internal revision is used for future purpose.
function Book(id, title, author) {
  // private fields
  this._id = id;
  this._title = title;
  this._author = author;
  this._comments = undefined;
  this._revision = 0;
  this._evaluations = [];

  // public methods
  this.getID = function() { return this._id; }
  this.getTitle = function() { return this._title; }
  this.getAuthor = function() { return this._author; }
  this.setComments = function(comments) { this._revision+=1; this._comments = comments; }
  this.getComments = function() { return this._comments; }
  this.setEvaluation = function(e) { this._evaluations[this._evaluations.length] = e; }
  this.getGlobalEvaluation = function() {
      var globalEvaluation = 0;

      for (var i = 0; i < this._evaluations.length; ++i) {
          globalEvaluation += this._evaluations[i];
      }
      return globalEvaluation / this._evaluations.length;
  }
}

// A Library contains books (collection of Book). It can gain or lose books.
// A method is used to search books by words contained in their titles.
// A method is used to barrow a book: checks are done and can take a while.
function Library() {
  this._books = [];

  this.addBook = function(book) {
      this._books[this._books.length] = book;
  }

  this.removeBook = function(id) {
      var bookIndex;
      for (var i = 0; i < this._books.length; ++i) {
          if (this._books[i].getID() == id) {
            bookIndex = i;
          }
      }

      if (bookIndex != undefined) {
          var removedBook = this._books.splice(bookIndex, 1);

          if (removedBook.length > 0) {
              return removedBook[0];
          }
      }
  }

  this.searchBooks = function() {
      var args = arguments;
      var books = [];

      for (var i = 0; i < this._books.length; ++i) {
          var title = this._books[i].getTitle();

          var matches = 0;
          for (var k = 0; k < args.length; ++k) {
              if (title.indexOf(args[k]) != -1) {
                  matches += 1;
              }
          }

          if (matches == args.length) {
              books[books.length] = this._books[i];
          }
      }

      return books;
  }

  this.barrowBook = function(book, user, onOperationSuccess, onOperationFailed) {

      function checkResult(success) {
          if (success) {
              onOperationSuccess(book);
          } else {
              onOperationFailed(error);
          }
      }

      // DO A LOT OF WORK..... NOTHING TO CHANGE HERE
      setTimeout(checkResult.bind(this), 3000, user.hasValidSubscription());
  }
}

function giveBarrowedBooksToTheReader(books) {
  // DO STUFF WITH THE BOOKS
  console.log("Thank you. Here are your " + books.length + " books");
}


/* ********************** EXAMPLE ****************************** */

var library = new Library();
var bookA = new Book(101, "La Barbe Bleue", "Charles Perrault");
var bookB = new Book(32, "La Petite Poucette", "Hans Chistian Andersen");
var bookC = new Book(35, "La Petite Sirene", "Hans Chistian Andersen");
var bookD = new Book(67, "Hansel et Gretel", "Jacob et Wilhelm Grimm");
var bookE = new Book(75, "La Petite gardeuse d'oies", "Jacob et Wilhelm Grimm");
var bookF = new Book(3, "Baba Yaga", "Alexandre Afanassiev");
library.addBook(bookA);
library.addBook(bookB);
library.addBook(bookC);
library.addBook(bookD);
library.addBook(bookE);
library.addBook(bookF);

bookC.setEvaluation(5);
bookC.setEvaluation(4);
bookC.setEvaluation(5);

library.removeBook(101);

console.log(bookC.getGlobalEvaluation()); // expected output: 4.666
console.log(library.searchBooks("Petite")); // expected output: [bookB, bookC, bookE]
console.log(library.searchBooks("Petite", "Sirene")); // expected output: [bookC]

// The reader barrows 2 books
var reader = {hasValidSubscription(){return true;}};

library.barrowBook(bookF, reader, function (book1) {
  console.log("Internal: successful operation on book: " + book1.getTitle());
  library.barrowBook(bookD, reader, function (book2) {
      console.log("Internal: successful operation on book: " + book2.getTitle());
      giveBarrowedBooksToTheReader([book1, book2]);
  }, function() {
      console.log("Internal: operation failed on book: " + book2.getTitle());
      giveBarrowedBooksToTheReader([book1]);
  });
}, function() {
  console.log("Internal: operation failed on book: " + book1.getTitle());
  giveBarrowedBooksToTheReader([]);
});

// expected output:
// Internal: successful operation on book: Baba Yaga
// Internal: successful operation on book: Hansel et Gretel
// Thank you. Here are your 2 books
